window.onload = function () {

    //instantiate new Vue objects
    //el links the element in the DOM to the Vue object
    //data holds data that belongs to the Object

    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello world!'
        }
    })

    var app2 = new Vue({
        el: '#app2',
        data: {
            message: 'You loaded this page on '+ new Date().toLocaleString()
        }
    })

    var app3 = new Vue({
        el: '#app3',
        data: {
            seen: true
        }
    })

    var app4 = new Vue({
        el: '#app4',
        data: {
            todos: [
                { text: 'Test'},
                { text: 'Test'},
                { text: 'Test'},
                { text: 'Test'},
                { text: 'Test'}
            ]
        }
    })

    var app5 = new Vue({
        el: '#app5',
        data: {
            message: 'Hello'
        },
        methods: {
            reverseMessage: function() {
                this.message = this.message.split('').reverse().join('')
            }
        }
    })

    var app6 = new Vue({
        el: '#app6',
        data: {
            message: 'Test'
        }
    })

    Vue.component('todo-item', {
        props: ['todo'],
        template: '<li>This is a todo</li>'
    })
}